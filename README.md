# keatool

A small program written in Go for managing host reservations to the kea database host table.

This is incomplete, unpolished, and the first time writing in Go.  

Licensed under GPLv2.  

Bulk Import File Format
***********************

The bulk file import format follows (plain text):

hostname,M:A:C:A:D:D:R:E:S:S,VLAN_ID,IP.AD.DR.ESS