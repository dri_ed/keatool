// keatool v0.7
// Ed Mitchell (ed.mitchell@dri.edu) 2020

// To Do:
// Modify Record functionality
// Set a limit to avoid handing out .240-.254 on subnets (reserved for network equipment and firewalls)

package main

import (
	"fmt"
	"bufio"
	"os"
	"strings"
	"bytes"
	"encoding/binary"
	"net"
	"database/sql"
	"sort"
	"log"
	_ "github.com/go-sql-driver/mysql"
)


type kea_entry struct {
	ipntoa string
	dehexmac string
	host_id string
	dhcp_id string
	dtype string
	vlan string
	vlan6 string
	ipaddr string
	hname string
	d4class string
	d6class string
	d4next string
	d4sname string
	d4bfn string
	ucontext string
	authkey string
	}

// declare some variables //

var vlan string
var hostname string
var ip string
var mac string
var rows int
var opt string
var debug bool
var verbose bool
var import_file string

// worker functions //

func Hosts(cidr string) ([]string, error) {
        ip, ipnet, err := net.ParseCIDR(cidr)
        if err != nil {
                return nil, err
        }

        var ips []string
        for ip := ip.Mask(ipnet.Mask); ipnet.Contains(ip); inc(ip) {
                ips = append(ips, ip.String())
        }
        // remove network address and broadcast address
        return ips[1 : len(ips)-1], nil
}

func inc(ip net.IP) {
        for j := len(ip) - 1; j >= 0; j-- {
                ip[j]++
                if ip[j] > 0 {
                        break
                }
        }
}

func nextfree(ip net.IP) (net.IP) {
        for j := len(ip) - 1; j >= 0; j-- {
                ip[j]++
                if ip[j] > 0 {
                        break
                }
        }
	return ip
}


func ip2Long(ip string) uint32 {
         var long uint32
         binary.Read(bytes.NewBuffer(net.ParseIP(ip).To4()), binary.BigEndian, &long)
         return long
}


func delete_record(del_ip string) sql.Result {
	fmt.Println("Deleting IP:", del_ip)
        db, err := sql.Open("mysql", "keaadmin:password@tcp(wizard.dri.edu)/kea")
	defer db.Close()
	stmt, err := db.Prepare("DELETE FROM hosts where (ipv4_address) = (INET_ATON(?)) ;")
	res,err := stmt.Exec(del_ip)
                if err!=nil{
                        log.Fatal(err)
                }
                if (debug) {
                        log.Println(res)
                }
                db.Close()
		return res
}


func hname_search(hostname string) {
	db, err := sql.Open("mysql", "keaadmin:password@tcp(wizard.dri.edu)/kea")
	defer db.Close()
	//results, err := db.Query("SELECT INET_NTOA(ipv4_address), HEX(dhcp_identifier), dhcp4_subnet_id, hostname FROM hosts where hostname =?", hostname)
	results, err := db.Query("SELECT INET_NTOA(ipv4_address), HEX(dhcp_identifier), dhcp4_subnet_id, hostname FROM hosts where hostname like ?", hostname)
	for results.Next() {
                        rows++
                        var tag kea_entry
                        err = results.Scan(&tag.ipntoa, &tag.dehexmac, &tag.hname, &tag.dhcp_id)
                                if err != nil {
                                        panic(err.Error())

                                }
			fmt.Println("----------------------------------------------------")
                        fmt.Println("|",tag.ipntoa,"|",tag.dehexmac,"|", tag.hname,"|",tag.dhcp_id,"|")
			fmt.Println("----------------------------------------------------")
			if (debug) {
                        	fmt.Println("Rows returned: ", rows)
			}
                }
	db.Close()
}

func mac_search(mac string) {
        db, err := sql.Open("mysql", "keaadmin:password@tcp(wizard.dri.edu)/kea")
        defer db.Close()
	fmt.Println("MAC search string is", mac)
	size := len(mac)
	fmt.Println("Length of search string is", size, "characters.")
        //results, err := db.Query("SELECT INET_NTOA(ipv4_address), HEX(dhcp_identifier), dhcp4_subnet_id, hostname FROM hosts where HEX(dhcp_identifier) =?", mac)
	results, err := db.Query("SELECT INET_NTOA(ipv4_address), HEX(dhcp_identifier), dhcp4_subnet_id, hostname FROM hosts where HEX(dhcp_identifier) =?", mac)
        for results.Next() {
                        rows++
                        var tag kea_entry
                        err = results.Scan(&tag.ipntoa, &tag.dehexmac, &tag.hname, &tag.dhcp_id)
                                if err != nil {
                                        panic(err.Error())

                                }
			fmt.Println("----------------------------------------------------")
                        fmt.Println("|",tag.ipntoa,"|",tag.dehexmac,"|",tag.hname,"|",tag.dhcp_id,"|")
			fmt.Println("----------------------------------------------------")
			if (debug) {
                       		fmt.Println("Rows returned: ",rows)
			}
                }

	db.Close()
}

func vlan_search(vlan string, allips []string) {
	var a int
	freeIPs := make([]net.IP, 0)
	foundIPs := make([]string, 0)
	byteIPs := make([]net.IP, 0)
	allbyteIPs := make([]net.IP, 0)
	db, err := sql.Open("mysql", "keaadmin:password@tcp(wizard.dri.edu)/kea")
        defer db.Close()
	if (debug) {
        	fmt.Println("VLAN search string is", vlan)
	}
        results, err := db.Query("SELECT INET_NTOA(ipv4_address), HEX(dhcp_identifier), dhcp4_subnet_id, hostname FROM hosts where dhcp4_subnet_id =?", vlan)
        for results.Next() {
                        rows++
                        var tag kea_entry
                        err = results.Scan(&tag.ipntoa, &tag.dehexmac, &tag.hname, &tag.dhcp_id)
                                if err != nil {
                                        panic(err.Error())

                                }
			if (debug) {
				fmt.Println("----------------------------------------------------")
                        	fmt.Println("|",tag.ipntoa,"|",tag.dehexmac,"|",tag.hname,"|",tag.dhcp_id,"|")
				fmt.Println("----------------------------------------------------")
			}
			foundIPs = append(foundIPs, tag.ipntoa)
			byteIPs = append(byteIPs, net.ParseIP(tag.ipntoa))
			//if (debug) {
			//	for x :=0; x <= (len(foundIPs)-1 ); x++ {
		        //		fmt.Println(foundIPs[x])
			//	}
			//}
			//if (debug) {
			//	fmt.Println("----------------------------------------------------")
                        //	fmt.Println("Rows returned: ",rows)
			//}
	 }
	 if (debug) {
         	a = len(foundIPs)
	 	fmt.Println("Found", a, "IPs in use.")
	 }

      	 sort.Slice(byteIPs, func(i, j int) bool {
		return bytes.Compare(byteIPs[i], byteIPs[j])  < 0
	 })
	 if (debug) {
	 	fmt.Println("IP address(es) in VLAN", vlan, "sorted in ascending order:")
	 	fmt.Println("------------------------------------------------------")
	 	for _, z := range byteIPs {
	  	 	fmt.Printf("%s\n", z)
	 	}
		fmt.Println("------------------------------------------------------")
	 }
	 // NOTE NOTE NOTE
	 // I think in here is where I would exclude the upper range of a subnet as reserved,
	 // per discussion with Ryan.
	 // Chew on this for a day and then try something!
	 for m := 0; m <= (len(allips) -1); m++ {
		allbyteIPs = append(allbyteIPs, net.ParseIP(allips[m]))
	 }
	 sort.Slice(allbyteIPs, func(b, d int) bool {
		return bytes.Compare(allbyteIPs[b], allbyteIPs[d]) < 0
	 })
	 for x := 0; x <= (len(byteIPs)-1); x++ {
		//for y := 0; y <= (len(allbyteIPs)-1); y++ {
		for y := 0; y <= x; y++ {
			compresult := bytes.Equal(allbyteIPs[y], byteIPs[x])
			// debugging stuff //
			//if (debug) {
			//	fmt.Println("allByte and byte slice lengths follow:")
			//	fmt.Println(len(allbyteIPs))
			//	fmt.Println(len(byteIPs))
			//}
			//if compresult {
			//	fmt.Println("This IP is in use:", byteIPs[x])
		 	//}
			if (!compresult) {
				//fmt.Println("This IP is NOT in use:", allbyteIPs[y])
				freeIPs = append(freeIPs, allbyteIPs[y])
			}
		}
	 }
	 total := cap(allbyteIPs)
	 fmt.Println(total)
	 q := len(byteIPs)
	 if ( q == 0 ) {
		 fmt.Println("There are no entries for VLAN", vlan)
		 os.Exit(99)
	 }
	 //fmt.Println("\n\nFirst free IP is:", byteIPs[q - 1])
	 ip := nextfree(byteIPs[q - 1])
	 subnet := get_subnet(vlan)
	 fmt.Println("The next free IP for subnet",subnet,"is:",ip)
	 db.Close()


}

func ip_search(ip string) {
	db, err := sql.Open("mysql", "keaadmin:password@tcp(wizard.dri.edu)/kea")
	defer db.Close()
	//fmt.Println("VLAN search string is", vlan)
	results, err := db.Query("SELECT INET_NTOA(ipv4_address), HEX(dhcp_identifier), dhcp4_subnet_id, hostname FROM hosts where INET_NTOA(ipv4_address) =?", ip)
	for results.Next() {
                       rows++
                       var tag kea_entry
	               err = results.Scan(&tag.ipntoa, &tag.dehexmac, &tag.hname, &tag.dhcp_id)
	                        if err != nil {
	                                panic(err.Error())

                        }
			fmt.Println("----------------------------------------------------")
	                fmt.Println("|",tag.ipntoa,"|",tag.dehexmac,"|",tag.hname,"|",tag.dhcp_id,"|")
			fmt.Println("----------------------------------------------------")
	}
	db.Close()

}

func insert_record(ip string, vlan string, hostname string, mac string) {
	// swap in 'err' for '_' below //
	var doit string = "y"
	if (debug) {
		var ans string = "y"
		fmt.Println(ans)
	}
	insReader := bufio.NewReader(os.Stdin)

	db, _ := sql.Open("mysql", "keaadmin:password@tcp(wizard.dri.edu)/kea")
        defer db.Close()
	fmt.Println("Inserting record with: IP: ", ip, "\nVLAN: ", vlan, "\nMAC address: ", mac, "\nHostname: ", hostname)
	fmt.Print("Is this what you want?")
	ans, _ :=insReader.ReadString('\n')
	ans = strings.Replace(ans, "\n", "", -1)
	strings.ToLower(ans)
	ans_result := ans == doit
	if ans_result {
		fmt.Println("Record accepted; adding to Kea hosts table.")
		stmt, err := db.Prepare("INSERT INTO hosts (dhcp_identifier,dhcp4_subnet_id,ipv4_address,hostname) VALUES (UNHEX(?),?,INET_ATON(?),?);")
		defer db.Close()
	        if err != nil {
        		panic(err.Error())
   		}
		if (debug) {
			fmt.Println("Using this value for MAC address:", mac)
		}
		res,err := stmt.Exec(mac, vlan, ip, hostname)
		if err!=nil{
 			log.Fatal(err)
 		}
		if (debug) {
			log.Println(res)
		}
		db.Close()
	} else {
	    fmt.Println("User has rejected the proposed record; exiting.")
	    os.Exit(99)
	}
}

func insert_bulk_record(ip string, vlan string, hostname string, mac string) {
        //insReader := bufio.NewReader(os.Stdin)
        db, _ := sql.Open("mysql", "keaadmin:password@tcp(wizard.dri.edu)/kea")
        defer db.Close()
        fmt.Println("Inserting record with: IP: ", ip, "\nVLAN: ", vlan, "\nMAC address: ", mac, "\nHostname: ", hostname)
        stmt, err := db.Prepare("INSERT INTO hosts (dhcp_identifier,dhcp4_subnet_id,ipv4_address,hostname) VALUES (UNHEX(?),?,INET_ATON(?),?);")
        defer db.Close()
        if err != nil {
                panic(err.Error())
        }
        fmt.Println("Using this value for MAC address:", mac)
        res,err := stmt.Exec(mac, vlan, ip, hostname)
        if err!=nil{
                log.Fatal(err)
        }
        log.Println(res)
        //db.Close()

}

func get_subnet(key string) string {
	        vlan_to_subnet := map[string]string{
			"1560": "10.15.60.0/23",
			"1008": "10.10.8.0/21",
			"1900": "10.19.0.0/23",
			"1800": "10.18.0.0/23",
			"1732": "10.17.32.0/23",
			"1730": "10.17.30.0/23",
			"1722": "10.17.22.0/23",
			"1720": "10.17.20.0/23",
			"1710": "10.17.10.0/23",
			"1700": "10.17.0.0/23",
			"1604": "10.16.4.0/23",
			"1602": "10.16.2.0/23",
			"1600": "10.16.0.0/23",
			"1552": "10.15.52.0/23",
			"1550": "10.15.50.0/23",
			"1540": "10.15.40.0/23",
			"1530": "10.15.30.0/23",
			"1520": "10.15.20.0/23",
			"1514": "10.15.14.0/23",
			"1510": "10.15.10.0/23",
			"1504": "10.15.4.0/23",
			"1502": "10.15.2.0/23",
			"1500": "10.15.0.0/23",
			"1100": "10.11.8.0/21",
		}
		subnet := vlan_to_subnet[key]
		return subnet
}

func main() {
        // Turn Debugging On Or Off //
	dbg := "d"
	vb := "v"
	if (len(os.Args) > 1  && len(os.Args) <= 3 ) {
		arg1 := os.Args[1]
		arg1 = strings.Replace(arg1, "-", "", -1)
		arg2 := os.Args[2]
		arg2 = strings.Replace(arg2, "-", "", -1)
		if (arg1 == dbg) || (arg2 == dbg) {
			debug = true
			//fmt.Println(debug,arg1)
		} else {
			debug = false
		}
		if (arg1 == vb) || (arg2 == vb) {
			verbose = true
			//fmt.Println(verbose,arg2)
		} else {
			verbose = false
		}
	}
	var allips []string
	var txtlines []string
	// Note: splitrecord will, with SplitN, split our records on the "," delimiter, on a per-line basis.
	// Then the splitrecord []string slice will hold in indices 0-3 all the data we need for a SQL insert.
	// I hope.
	var splitrecord []string
        fmt.Println("Connecting to kea database; table hosts\n")
	// Ask the User For Stuff and Things //
	reader := bufio.NewReader(os.Stdin)
	if (debug) {
		fmt.Println("Keatool v4.01, (c) 2020")
	}
	fmt.Print("1) Search by IP Address\n")
	fmt.Print("2) Search by MAC Address\n")
	fmt.Print("3) Search by Host Name\n")
	fmt.Print("4) Search by VLAN ID[long list]\n")
	fmt.Print("5) Add a new record (need IP, hostname, VLAN, and MAC address)\n")
	fmt.Print("6) Search for a free IP address\n")
	fmt.Print("7) Modify an existing record -- ** NOT IMPLEMENTED YET **\n")
	fmt.Print("8) Add records via file\n")
	fmt.Print("9) Get IP subnet and mask from VLAN lookup\n")
	fmt.Print("d) D(elete) a record by IP address\n")
	fmt.Print("q) Quit\n")
	fmt.Print("Enter an option: ")
	opt, _ := reader.ReadString('\n')
	opt = strings.Replace(opt, "\n", "", -1)



	// Start the switch statement here //

	switch opt {

	case "1":
		fmt.Print("Enter an IP address: ")
		ip, _ := reader.ReadString('\n')
		ip = strings.Replace(ip, "\n", "", -1)
		ip_search(ip)
	case "2":
		fmt.Print("Enter a MAC address: ")
		mac, _ := reader.ReadString('\n')
		mac = strings.Replace(mac, "\n", "", -1)
		// Adding to handle ":" - why can't strings.Replace handle multiple characters?  //
		//mac = strings.Replace(mac, ":", "", -1)
		mac_search(mac)
	case "3":
		fmt.Print("Enter a hostname: ")
		hostname, _ := reader.ReadString('\n')
		hostname = strings.Replace(hostname, "\n", "%", -1)
		hname_search(hostname)
	case "4":
		fmt.Print("Enter a VLAN ID: ")
		vlan, _ := reader.ReadString('\n')
		vlan = strings.Replace(vlan, "\n", "", -1)
		subnet := get_subnet(vlan)
		allips,_ = Hosts(subnet)
		//fmt.Println(allips)
		if (debug) {
			fmt.Println("Subnet for VLAN",vlan,"is",subnet)
		} else {
			fmt.Println("Working with VLAN",vlan)
		}
		vlan_search(vlan, allips)
	case "5":
		fmt.Print("Enter IP address: ")
		ip, _ := reader.ReadString('\n')
		ip = strings.Replace(ip, "\n", "", -1)
		longIP := ip2Long(ip)
		fmt.Print("Enter MAC address: ")
		mac, _ := reader.ReadString('\n')
		mac = strings.Replace(mac, "\n", "", -1)
		mac = strings.Replace(mac, ":" ,"", -1)
		macbyte := []byte(mac)
		if (debug) {
			fmt.Println("IP-as-long:", longIP)
			fmt.Println("MAC address as bytes:",macbyte)
		}
		fmt.Print("Enter VLAN ID: ")
		vlan, _ := reader.ReadString('\n')
		vlan = strings.Replace(vlan, "\n", "", -1)
		fmt.Print("Enter hostname: ")
		hostname, _ := reader.ReadString('\n')
		hostname = strings.Replace(hostname, "\n", "", -1)
		insert_record(ip, vlan, hostname, mac)
	case "6":
		fmt.Print("Enter a subnet to search with mask (a.b.c.d/nn):")
		searchnet, _ := reader.ReadString('\n')
		searchnet = strings.Replace(searchnet, "\n", "", -1)
		fmt.Println(searchnet)
		hosts, _ := Hosts(searchnet)
		fmt.Println(len(hosts))
	case "7":
		fmt.Println("Placeholder for record modification")
		fmt.Print("Enter the MAC address of the host record: ")
		mac, _ := reader.ReadString('\n')
		mac = strings.ReplaceAll(mac, "\n", "")
		mac = strings.ReplaceAll(mac, ":", "")
		if (debug) {
			fmt.Println(mac)
		}
		os.Exit(0)
	case "8":
		fmt.Print("Enter a filename to import records:")
		import_file,_  = reader.ReadString('\n')
		import_file = strings.Replace(import_file, "\n", "", -1)
		file, err := os.Open(import_file)
		if err != nil {
			log.Fatalf("failed opening file: %s", err)
		}
		scanner := bufio.NewScanner(file)
		scanner.Split(bufio.ScanLines)
		for scanner.Scan() {
			txtlines = append(txtlines, scanner.Text())
		}
		file.Close()
		if (debug) {
			for _, one_line := range txtlines {
				fmt.Println(one_line)
			}
		}
		for f := 0; f <= (len(txtlines)-1); f++ {
			splitrecord = strings.SplitN(txtlines[f], ",", -1)
			splitrecord[1] = strings.Replace(splitrecord[1], ":" ,"", -1)
			insert_bulk_record(splitrecord[3], splitrecord[2], splitrecord[0], splitrecord[1])
			}
		os.Exit(0)
	case "9":
                fmt.Print("Enter a VLAN ID: ")
                vlan, _ := reader.ReadString('\n')
                vlan = strings.Replace(vlan, "\n", "", -1)
                subnet := get_subnet(vlan)
		_, ipv4Net, _ := net.ParseCIDR(subnet)
		mask := ipv4Net.Mask
                fmt.Println("Subnet for VLAN",vlan,"is",subnet)
		fmt.Print("Decimal-format subnet mask is: ")
		fmt.Println(fmt.Sprintf("%d.%d.%d.%d", mask[0], mask[1], mask[2], mask[3]))
	case "d":
		fmt.Print("Enter an IP address: ")
		del_ip, _ := reader.ReadString('\n')
		del_ip = strings.Replace(del_ip, "\n", "", -1)
		fmt.Println("IP to delete:", del_ip)
		os.Exit(126)
		deleted := delete_record(del_ip)
		fmt.Println(deleted)
	case "q":
		os.Exit(1)
	default:
		os.Exit(99)
	}

	rows = 0 // init the rows value //


}